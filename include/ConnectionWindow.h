/**
 * @file ConnectionWindow.h
 * @author Daniel Świetlik
 * @brief Header przechowujacy klase obiektu okna polaczenia
 * @version 0.1
 * @date 2022-05-17
 *
 * @copyright Copyright (c) 2022
 *
 */
#pragma once

#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/choice.h>
#include <wx/panel.h>
#include <wx/notebook.h>

#include <iostream>
#include <memory>

#include "./Serial/SerialPort.h"
#include "./Serial/SerialEnums.h"
#include "./Serial/Exceptions.h"
#include "MainWindow.h"
#include "ReadingThread.h"

#define WRITE_BUTTON_ID 98
#define START_READING_BUTTON_ID 97
#define END_READING_BUTTON_ID 96
#define CLEAR_TEXT_BUTTON_ID 95

/**
 * @brief Klasa okna, ktore pozwala na przyjmowania i wysylanie wiadomosci z/do portu szeregowego
 *
 */
class ConnectionWindow : public wxFrame {

public:
    /**
     * @brief Construct a new Connection Window object
     *
     * @param title tytul okna
     * @param size rozmiar okna
     */
    ConnectionWindow(const wxString &title, const wxSize &size, std::shared_ptr<Serial::SerialPort> connectedPort);
    /**
     * @brief Event, ktory wykonuje sie na zamknieciu okna polaczenia
     *
     * @param event obiekt eventu przechowujacy rozne informacje dotyczace go
     */
    virtual void OnClose(wxCloseEvent &event);
    /**
     * @brief Event obslugujacy nacisniejcie dowolnego z przyciskow
     *
     * @param event obiekt eventu przechowujacy rozne informacje dotyczace go
     */
    void onButtonClicked(wxCommandEvent &event);
    /**
     * @brief Event, ktory przyjmuje dane z watku czytajacego dane z portu i zapisuje je do pola tekstowego
     *
     * @param event obiekt eventu przechowujacy rozne informacje dotyczace go
     */
    void onReadingUpdated(wxCommandEvent &event);
    /**
     * @brief Event, ktory wywoluje sie na zakonczenie watku czytajacego dane z portu
     *
     * @param event obiekt eventu przechowujacy rozne informacje dotyczace go
     */
    void onReadingCompleted(wxCommandEvent &event);
    /**
     * @brief Metoda, wywolywana na wcisniecie przycisku czytaj dane, uruchamia watek czytajacy dane
     *  z portu lub go zatrzymuje
     *
     */
    void readData();
    /**
     * @brief Metoda, wywolywana przez wcisniecie przycisku wyslij dane,
     * wysyla wiadomosc w polu tekstowym do portu
     *
     */
    void writeData();

protected:
    /**
     * @brief Obiekt polaczonego portu szeregowego
     */
    std::shared_ptr<Serial::SerialPort> _connectedPort = nullptr;
    /**
     * @brief Obiekt z polem tekstowym do wysylania danych
     */
    wxTextCtrl *_sendTextBox;
    /**
     * @brief Obiekt z polem tekstowym do przyjmowania danych
     */
    wxTextCtrl *_readTextBox;
    /**
     * @brief Przycisk wywolujacy odczytywanie danych z portu
     */
    wxButton *_startReading;
    /**
     * @brief Zmienna przechowujaca informacje czy program czyta dane z portu
     */
    bool _isReading = false;
    /**
     * @brief Obiekt wątku odczytujacy dane z portu szeregowego
     *
     */
    ReadingThread *_thread;
    /**
     * @brief Obiekt sekcji krytycznej dla watku czytelnika
     *
     */
    wxCriticalSection _threadCS;
};
