/**
 * @file ReadingThread.h
 * @author Daniel Swietlik
 * @brief Header przechowujacy klase watku do odczytywania danych wyslanych przez polaczony port szeregowy
 * @version 0.1
 * @date 2022-05-17
 *
 * @copyright Copyright (c) 2022
 *
 */
#pragma once

#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/choice.h>
#include <wx/panel.h>
#include <wx/notebook.h>

#include <iostream>
#include <memory>

#include "./Serial/SerialPort.h"
#include "./Serial/SerialEnums.h"
#include "./Serial/Exceptions.h"

#define READING_UPDATED 10000
#define READING_COMPLETED 10001

/**
 * @brief Klasa watku, ktory odczytuje dane z polaczonego portu szeregowego
 *
 */
class ReadingThread : public wxThread {

public:
    /**
     * @brief Construct a new Reading Thread object
     *
     * @param handler Rodzic, wywolujacy watek
     * @param port port szeregowy z ktorego ma czytac watek
     */
    ReadingThread(wxFrame *handler, std::shared_ptr<Serial::SerialPort> port);
    /**
     * @brief Destroy the Reading Thread object
     *
     */
    ~ReadingThread();

protected:
    /**
     * @brief Metoda wykonujaca prace watku
     *
     * @return ExitCode
     */
    virtual ExitCode Entry();

protected:
    /**
     * @brief Obiekt rodzica watku
     *
     */
    wxFrame *_handler;
    /**
     * @brief Obiekt polaczonego portu szeregowego do odczytania
     *
     */
    std::shared_ptr<Serial::SerialPort> _port = nullptr;
};
