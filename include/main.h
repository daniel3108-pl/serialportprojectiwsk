/**
 * @file main.h
 * @author Daniel Swietlik
 * @brief Header przechowujacy klase wejsciowa aplikacji
 * @version 0.1
 * @date 2022-05-17
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once

#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <wx/wx.h>

#include "../include/Serial/SerialPort.h"
#include "../include/MainWindow.h"

/**
 * @brief Klasa wejściowa dla aplikacji GUI
 * 
 */
class App : public wxApp {
    public:
        /**
         * @brief Metoda wejsciowa aplikacji
         * 
         * @return true 
         * @return false 
         */
        virtual bool OnInit();
};