#pragma once

#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/choice.h>
#include <wx/radiobox.h>
#include <wx/dialog.h>
#include <wx/slider.h>

#include <iostream>
#include <memory>
#include <format>
#include <string>
#include <string_view>

#include <boost/format.hpp>

#include "./Serial/SerialPort.h"
#include "./Serial/SerialEnums.h"
#include "./Serial/Exceptions.h"
#include "ConnectionWindow.h"


#define CONNECT_BUTTON_ID 100
#define PING_BUTTON_ID    101
#define REFRESH_BUTTON_ID 102

/**
 * @brief Klasa głownego okna aplikacji gui
 * 
 */
class MainWindow : public wxFrame {

public:
    /**
     * @brief Construct a new Main Window object
     * 
     * @param title tytul okna
     * @param size rozmiar okna
     */
    MainWindow(const wxString& title, const wxSize& size);
    /**
     * @brief Przetwarza uzycie przycisku w gui
     * 
     * @param event 
     */
    void onButtonClicked(wxCommandEvent &event);
    /**
     * @brief Laczy sie z portem o podanych parametrach
     * 
     */
    void makeConnection();
    /**
     * @brief Pinguje wybrany port szeregowy
     * 
     */
    void pingPort();
    /**
     * @brief Refreshuje liste dostepnych portow
     * 
     */
    void refreshPortList();
    
protected:

    wxChoice *portSelector = nullptr;
    wxTextCtrl *baudRateBox = nullptr;
    wxRadioBox *charFormat = nullptr;
    wxRadioBox *charControl = nullptr;
    wxRadioBox *charStop = nullptr;
    wxRadioBox *flowCtrl = nullptr;
    wxRadioBox *terminatorCtrl = nullptr;

    std::shared_ptr<Serial::SerialPort> _connectedPort = nullptr;

};