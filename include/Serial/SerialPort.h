/**
 * @file SerialPort.h
 * @author Daniel Świetlik @daniel3108-pl
 * @brief Plik nagłowkowy dla klasy SerialPort, ktory zajmuje sie obsluga portu szeregowego,
 *          klasa ta wykonuje odpowiednia komunikacje z portem uzywając UNIX'owych funkcji i syscalli
 *          z tego tez powodu klasa ta nie zadziala systemach Windows,
 *          zadziala z kolei na systemach Linux oraz macOS
 * @version 0.1
 * 
 * @copyright Copyright (c) 2022 Informatyka Katowice AEiI Grupa 2 Sekcja 2
 * 
 */

#pragma once 

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>
#include <chrono>
#include <vector>
#include <filesystem>
#include <regex>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "./SerialEnums.h"
#include "./Exceptions.h"


/**
 * @brief Przestrzeń nazw dla klas i funkcji związanych z obsługą portu szeregowego
 * 
 */
namespace Serial {

/**
 * @brief Klasa odpowiedzialna za tworzenie połączenia i 
 * komunikację z portem szeregowym
 */
class SerialPort {
public:
    /**
     * @brief Construct a new Serial Port object
     * 
     * @param name Nazwa portu w katalogu /dev/
     * @param timeout Czas w mikrosekundach po ktorym nawiazanie polaczenia (np. ping) po nie otrzymaniu odpwiedzi sie zamyka
     */
    SerialPort(const std::string name, const double timeout);
    /**
     * @brief Construct a new Serial Port object
     * 
     * @param port obiekt do przekopiowania
     */
    SerialPort(const SerialPort& port);
    /**
     * @brief Destroy the Serial Port object
     */
    ~SerialPort();
    
    #pragma region Funkcjonalność obsługi portu

    /**
     * @brief Otwiera połączenie z portem szeregowym o podanej nazwie
     * 
     * @return odpowiedni kod bledu z enumu SerialPortErrorCode
     */
    SerialPortErrorCode openPort();
    /**
     * @brief Konfiguracja połączenia z portem szeregowym z domyslnymi ustawieniami polaczenia
     * 
     * @param bitrate Bitrate odczytywania lub wpisywania bitow
     * @return odpowiedni kod bledu z enumu SerialPortErrorCode
     */
    SerialPortErrorCode configurePort(const int bitrate);

    /**
     * @brief Konfigurowanie połączenia z portem szeregowym z uyciem wybranych własnych parametrow 
     * konfiguracyjnych.
     * 
     * @param baudrate wartosc baudratu na wejsciu/wyjsciu
     * @param flControl ustawienia dotyczace flow control portu
     * @param terminator ustawienia terminatora
     * @param chFormat ustawienie ilosci bitow danych
     * @param chControl ustawienie sprawdzania bitu parzystosci
     * @param chStops ustawienie stop bitow
     * @return odpowiedni kod bledu z enumu SerialPortErrorCode
     */
    SerialPortErrorCode configurePortWithCustomSettings(const unsigned int baudrate, 
                                         SerialFlowControl flControl, 
                                         SerialPortTerminator terminator,
                                         SerialCharacterFormat chFormat,
                                         SerialCharacterControl chControl,
                                         SerialCharacterStopBits chStops);
    /**
     * @brief Zamyka połączenie z portem
     * 
     * @return true jeśli się udało zamknąć
     * @return false jeśli się nie udało zamknąć
     */
    bool closePort();

    /**
     * @brief Pinguje port szeregowy i liczy czas Round-trip delay
     * 
     * @return double czas rtd w mikrosekundach lub -1.0 jeśli nie udało się zpingować
     * @throws SerialPortNotOpenException gdy nie otwarto portu i nie skonfigurowano go
     */
    double ping();

    /**
     * @brief Metoda, ktora czysci otrzymane wczesniej z portu wiadomosci
     * 
     * @return true jesli sie udalo wyczyscic
     * @return false jesli sie nie udalo wyczyscic
     * @throws SerialPortNotOpenException gdy nie otwarto portu i nie skonfigurowano go
     */
    bool clearMessages();
    /**
     * @brief odczytuje wiadomość wysłaną z portu z bufforem o długości 255 znakow
     * 
     * @return std::string wiadomość z portu lub "" gdy nie udało się odczytać zadnej wiadomości
     */
    std::string readMessage();
    /**
     * @brief odczytuje wiadomość o długości podanej poniej
     * 
     * @param bytes ilosc bajtow do odczytania
     * @return std::string wiadomosc odczytana z portu lub "" gdy nie udało się nic odczytac
     * @throws SerialPortNotOpenException gdy nie otwarto portu i nie skonfigurowano go
     */
    std::string readMessage(const int bytes);
    /**
     * @brief Odczytuje bajty wysłane z portu o bufforze 255
     * 
     * @return unsigned* bajty wysłane z portu 
     * @throws SerialPortNotOpenException gdy nie otwarto portu i nie skonfigurowano go
     */
    unsigned char* readBytes();
    /**
     * @brief Odczytuje bajty wysłana z portu o podanym bufforze
     * 
     * @param bytes ilosc bytow do otrzymania
     * @return unsigned* bajty wyslane z portu 
     * @throws SerialPortNotOpenException gdy nie otwarto portu i nie skonfigurowano go
     */
    unsigned char* readBytes(const int bytes);
    /**
     * @brief Wysłanie wiadomości z tekstem do portu
     * 
     * @param message string z wiadomością
     * @return true jeśli udało się przesłać wiadomość
     * @return false jeśli się nie udało przesłać wiadomości
     * @throws SerialPortNotOpenException gdy nie otwarto portu i nie skonfigurowano go
     */
    bool writeMessage(const std::string& message);

    /**
     * @brief Checks if port is available
     * 
     * @return int 
     */
    int available();

    /**
     * @brief Skanuje wszystkie dostępne porty szeregowe
     * 
     * @return std::vector<std::string> wektor z nazwami portow w katalogu /dev/
     */
    static std::vector<std::string> scanAvailablePorts();

    #pragma endregion

    #pragma region Gettery Settery
    
    /**
     * @brief Get the Port Name object
     * 
     * @return std::string 
     */
    std::string getPortName();
    /**
     * @brief Get the Bitrate object
     * 
     * @return int 
     */
    int getBitrate();
    /**
     * @brief Get the Com object
     * 
     * @return int 
     */
    int getCom();
    /**
     * @brief Get the Serial Port Options object
     * 
     * @return struct termios 
     */
    struct termios getSerialPortOptions();

    #pragma endregion

protected:
    /**
     * @brief Nazwa portu w katalogu /dev/ (np. ttyS1 lub ttys002)
     */
    std::string _portName;
    /**
     * @brief Id uchwytu do portu szeregowego
     */
    int _com;
    /**
     * @brief Ustwaienia portu szeregowego
     */
    struct termios _serialPortOptions;
    /**
     * @brief Timeout dla połączenia
     * 
     */
    double _timeout; 
    /**
     * @brief Terminator dla portu
     * 
     */
    std::string _terminator;
    /**
     * @brief Odczytana wiadomosc przed otrzymaniem terminatora
     * przy pojawieniu sie terminatora jest konkatenowana z ostatnia otrzymana wiadomoscia
     */
    std::string _currentMessageBeforeTerminator = "";
};

}