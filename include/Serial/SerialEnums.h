/**
 * @file SerialEnums.h
 * @brief Plik nagłowkowy z enumami zwiazanymi z klasa SerialPort zwiazanych z ustawieniami portu szeregowego
 * @version 0.1
 * 
 * @copyright Copyright (c) 2022 Informatyka Katowice AEiI Grupa 2 Sekcja 2
 * 
 */
#pragma once 

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>
#include <chrono>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

namespace Serial {

    /**
     * @brief Symbolizuje ustawienie terminatora dla portu szeregowego
     * 
     */
    enum SerialPortTerminator {
        NO_TERMINATOR = 0,
        CR,
        LF,
        CR_LF
    };

    /**
     * @brief Symbolizuje ustawienia kontroli przeplywu portu szeregowego
     * 
     */
    enum SerialFlowControl {
        NO_CONTROL = 0,
        HARDWARE_DTR_DSR,
        HARDWARE_RTS_CTS,
        SOFTWARRE_XON_XOFF
    };

    /**
     * @brief Symbolizuje format znaku danych dla portu szeregowego
     * 
     */
    enum SerialCharacterFormat {
        SEVEN_BITS = 0,
        EIGHT_BITS
    };

    /**
     * @brief Symbolizuje kontrole formatu znaku danych dla portu szeregowego
     * 
     */
    enum SerialCharacterControl {
        EVEN = 0,
        ODD,
        NONE,
    };

    /**
     * @brief Symbolizuje ile stop bitow uzyc
     * 
     */
    enum SerialCharacterStopBits {
        ONE = 0,
        TWO,
    };

    /**
     * @brief Symbolizuje błędy portu
     * 
     */
    enum SerialPortErrorCode {
        NO_SUCH_PORT = 0,
        PORT_BUSY,
        CONFIGURATION_ERROR,
        NO_ERROR
    };

}