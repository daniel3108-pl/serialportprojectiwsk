#pragma once

#include <exception>
#include <string>

namespace Serial {

    class SerialPortNotOpenException : public std::exception {
        public:
            SerialPortNotOpenException(std::string message) throw()
                : _message(message)
            {
            }

            const char * what() const throw() {
                return this->_message.c_str();
            }

        protected:
            std::string _message = "";
    };

    class SerialPortCouldNotBeOpenedException : public std::exception {
        public:
            SerialPortCouldNotBeOpenedException(std::string message) throw()
                : _message(message)
            {
            }

            const char * what() const throw() {
                return this->_message.c_str();
            }

        protected:
            std::string _message = "";
    };

}