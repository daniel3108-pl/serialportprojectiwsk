#include "../include/ConnectionWindow.h"


ConnectionWindow::ConnectionWindow(const wxString& title, const wxSize& size, std::shared_ptr<Serial::SerialPort> connectedPort)
    : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, size), _connectedPort(connectedPort)
{
    wxPanel *panel = new wxPanel(this, wxID_ANY);

    wxNotebook* notebook = new wxNotebook(panel, wxID_ANY);
    
    wxPanel* readDataPanel = new wxPanel(notebook, wxID_ANY);
    wxBoxSizer* readDataSizer = new  wxBoxSizer(wxVERTICAL);

    _readTextBox = new wxTextCtrl(readDataPanel, wxID_ANY, "", wxDefaultPosition, wxSize(800, 550), wxTE_MULTILINE);
    _readTextBox->SetEditable(false);
    readDataSizer->Add(_readTextBox, wxEXPAND | wxALL, 5);

    wxBoxSizer* rButtonSizer = new wxBoxSizer(wxHORIZONTAL);

    _startReading = new wxButton(readDataPanel, START_READING_BUTTON_ID, wxT("Start reading data"), wxDefaultPosition, wxSize(130, 50));
    _startReading->Bind(wxEVT_BUTTON, &ConnectionWindow::onButtonClicked, this);
    
    wxButton* clearBtn = new wxButton(readDataPanel, CLEAR_TEXT_BUTTON_ID, wxT("Clear"), wxDefaultPosition, wxSize(130, 50));
    clearBtn->Bind(wxEVT_BUTTON, &ConnectionWindow::onButtonClicked, this);
    
    rButtonSizer->Add(_startReading, wxALIGN_RIGHT | wxRIGHT, 2);
    rButtonSizer->Add(clearBtn, wxALIGN_RIGHT | wxRIGHT, 2);

    readDataSizer->Add(rButtonSizer);

    readDataPanel->SetSizer(readDataSizer);    
    notebook->AddPage(readDataPanel, "Read data");
    
    
    wxPanel* writeDataPanel = new wxPanel(notebook, wxID_ANY);
    wxBoxSizer* writeDataSizer = new  wxBoxSizer(wxVERTICAL);
    wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

    wxButton* writeBtn = new wxButton(writeDataPanel, WRITE_BUTTON_ID, wxT("Send data"), wxDefaultPosition, wxSize(80, 50));
    writeBtn->Bind(wxEVT_BUTTON, &ConnectionWindow::onButtonClicked, this);

    buttonSizer->Add(writeBtn, wxALIGN_RIGHT | wxRIGHT, 2);

    _sendTextBox = new wxTextCtrl(writeDataPanel, wxID_ANY, "", wxDefaultPosition, wxSize(800, 550), wxTE_MULTILINE);
    writeDataSizer->Add(_sendTextBox, wxEXPAND | wxALL, 5);
    writeDataSizer->Add(buttonSizer);
    writeDataPanel->SetSizer(writeDataSizer);    

    notebook->AddPage(writeDataPanel, "Send data");

    wxBoxSizer* panelSizer = new wxBoxSizer(wxHORIZONTAL);
    panelSizer->Add(notebook, 1, wxEXPAND);
    panel->SetSizer(panelSizer);

    wxBoxSizer* topSizer = new wxBoxSizer(wxHORIZONTAL);
    topSizer->SetMinSize(250, 100);
    topSizer->Add(panel, 1, wxEXPAND);
    SetSizerAndFit(topSizer);

    this->Bind(wxEVT_COMMAND_TEXT_UPDATED, &ConnectionWindow::onReadingUpdated, this, READING_UPDATED);
    this->Bind(wxEVT_CLOSE_WINDOW, &ConnectionWindow::OnClose, this);
}

void ConnectionWindow::onButtonClicked(wxCommandEvent &event) {
    switch (event.GetId()) {
        case WRITE_BUTTON_ID:
            writeData();
            break;
        case START_READING_BUTTON_ID:
            readData();
            break;
        case CLEAR_TEXT_BUTTON_ID:
            _readTextBox->SetValue("");
            break;
    }
}

void ConnectionWindow::OnClose(wxCloseEvent& event) {
    _connectedPort->closePort();

    if (_isReading) {
        if (_thread->Delete() != wxTHREAD_NO_ERROR)
            wxLogError("Can't delete the thread!");
    }

    Destroy();
}

void ConnectionWindow::onReadingUpdated(wxCommandEvent& event) {
    if (event.GetString().length() > 0)
        _readTextBox->AppendText(event.GetString());
}

void ConnectionWindow::onReadingCompleted(wxCommandEvent& event) {

}

void ConnectionWindow::readData() {
    if (!_isReading) {
        _thread = new ReadingThread(this, _connectedPort);

        if (_thread->Run() != wxTHREAD_NO_ERROR) {
            wxLogError("Can't create the thread!");
            delete _thread;
            _thread = NULL;
        }
        _isReading = true;
        _startReading->SetLabel("Stop reading data");
    } else {
        if (_thread->Delete() != wxTHREAD_NO_ERROR)
            wxLogError("Can't delete the thread!");

        _startReading->SetLabel("Start reading data");
        _isReading = false;
    }
}

void ConnectionWindow::writeData() {
    std::string dataToWrite(this->_sendTextBox->GetValue().mb_str());

    _connectedPort->writeMessage(dataToWrite);
}

