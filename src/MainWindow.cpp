#include "../include/MainWindow.h"


MainWindow::MainWindow(const wxString& title, const wxSize& size)
    : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, size)
{
    Centre();

    wxStaticBoxSizer *sizer = new wxStaticBoxSizer(wxVERTICAL, this, "Serial Configuration");

    wxStaticText *text = new wxStaticText(this, wxID_ANY, wxT("Choose serial port:") ,wxDefaultPosition, wxSize(120, 20));

    sizer->Add(text, 0, wxALIGN_LEFT);

    portSelector = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxSize(600, 50));

    std::vector<std::string> serialPorts = Serial::SerialPort::scanAvailablePorts();
    int i = 0;
    for (std::string file : serialPorts) {
        portSelector->Insert(file, i++);
    }
    
    sizer->Add(portSelector, 0, wxEXPAND | wxALL, 5);

    wxStaticText *text2 = new wxStaticText(this, wxID_ANY, wxT("Choose baude rate:") ,wxDefaultPosition, wxSize(120, 20));

    sizer->Add(text2, 0, wxALIGN_LEFT);

    baudRateBox = new wxTextCtrl(this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(600, 30));
    sizer->Add(baudRateBox, 0, wxEXPAND | wxBOTTOM, 5);

    charFormat = new wxRadioBox();
    wxArrayString choices;
    choices.Add("7 bytes");
    choices.Add("8 bytes");
    charFormat->Create(this, wxID_ANY, "Character Format", wxDefaultPosition, wxSize(600, 50), choices);

    sizer->Add(charFormat, 0, wxEXPAND | wxBOTTOM, 5);


    charControl = new wxRadioBox();
    wxArrayString choices2;
    choices2.Add("Even");
    choices2.Add("Odd");
    choices2.Add("None");
    charControl->Create(this, wxID_ANY, "Character Format Control", wxDefaultPosition, wxSize(600, 50), choices2);

    sizer->Add(charControl, 0, wxEXPAND | wxBOTTOM, 5);

    charStop = new wxRadioBox();
    wxArrayString choices3;
    choices3.Add("One");
    choices3.Add("Two");
    charStop->Create(this, wxID_ANY, "Character Stop Bits", wxDefaultPosition, wxSize(600, 50), choices3);

    sizer->Add(charStop, 0, wxEXPAND | wxBOTTOM, 5);

    flowCtrl = new wxRadioBox();
    wxArrayString choices4;
    choices4.Add("No Control");
    choices4.Add("Hardware DTR DSR");
    choices4.Add("Hardware RTS CTS");
    choices4.Add("Software XON XOFF");
    flowCtrl->Create(this, wxID_ANY, "Flow Control", wxDefaultPosition, wxSize(600, 120), choices4, 4, wxVERTICAL);

    sizer->Add(flowCtrl, 0, wxEXPAND | wxBOTTOM, 5);

    terminatorCtrl = new wxRadioBox();
    wxArrayString choices5;
    choices5.Add("No Terminator");
    choices5.Add("CR");
    choices5.Add("LF");
    choices5.Add("CR/LF");
    terminatorCtrl->Create(this, wxID_ANY, "Terminator", wxDefaultPosition, wxSize(600, 70), choices5, 4, wxHORIZONTAL);

    sizer->Add(terminatorCtrl, 0, wxEXPAND | wxBOTTOM, 5);

    wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);

    wxButton *refreshBtn = new wxButton(this, REFRESH_BUTTON_ID, wxT("Refresh ports list"), wxDefaultPosition, wxSize(120, 50));
    // Podpiecie metody onButtonClicked do przycisku refreshBtn
    refreshBtn->Bind(wxEVT_BUTTON, &MainWindow::onButtonClicked, this);

    btnSizer->Add(refreshBtn, 0, wxRIGHT, 5);

    wxButton *pingBtn = new wxButton(this, PING_BUTTON_ID, wxT("Ping"), wxDefaultPosition, wxSize(80, 50));
    // Podpiecie metody onButtonClicked do przycisku pingBtn
    pingBtn->Bind(wxEVT_BUTTON, &MainWindow::onButtonClicked, this);

    btnSizer->Add(pingBtn, 0, wxRIGHT, 5);

    wxButton *connectBtn = new wxButton(this, CONNECT_BUTTON_ID, wxT("Connect"), wxDefaultPosition, wxSize(80, 50));
    connectBtn->Bind(wxEVT_BUTTON, &MainWindow::onButtonClicked, this);

    btnSizer->Add(connectBtn, 0);

    sizer->Add(btnSizer, 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM, 5);

    this->SetSizerAndFit(sizer);

}

void MainWindow::onButtonClicked(wxCommandEvent &event) {

    switch (event.GetId()) {
        case CONNECT_BUTTON_ID:
            makeConnection();
            break;
        case PING_BUTTON_ID:
            pingPort();
            break;
        case REFRESH_BUTTON_ID:
            refreshPortList();
            break;
    }
    
    event.Skip();
}

void MainWindow::makeConnection() {
    double rate = 0.0;
    try {
         rate = std::stod(std::string(baudRateBox->GetValue().mb_str()), nullptr);
    } catch (std::invalid_argument& ex) {
        wxMessageDialog *errordia = new wxMessageDialog(NULL, wxT("Baudrate in incorrect format"), wxT("Error"), wxOK | wxICON_ERROR);
        errordia->ShowModal();   
        return;
    }

    Serial::SerialFlowControl flControl;
    Serial::SerialPortTerminator terminator;
    Serial::SerialCharacterFormat chFormat;
    Serial::SerialCharacterControl chControl;
    Serial::SerialCharacterStopBits chStops;

    switch (flowCtrl->GetSelection()) {
        case 0:
            flControl = Serial::SerialFlowControl::NO_CONTROL;
            break;
        case 1:
            flControl = Serial::SerialFlowControl::HARDWARE_DTR_DSR;
            break;
        case 2:
            flControl = Serial::SerialFlowControl::HARDWARE_RTS_CTS;
            break;
        case 3:
            flControl = Serial::SerialFlowControl::SOFTWARRE_XON_XOFF;
            break;
    }

    switch (charStop->GetSelection()) {
        case 0:
            chStops = Serial::SerialCharacterStopBits::ONE;
            break;
        case 1:
            chStops = Serial::SerialCharacterStopBits::TWO;
            break;
    }

    //even odd none
    switch (charControl->GetSelection()) {
        case 0:
            chControl = Serial::SerialCharacterControl::EVEN;
            break;
        case 1:
            chControl = Serial::SerialCharacterControl::ODD;
            break;
        case 2:
            chControl = Serial::SerialCharacterControl::NONE;
            break;
    }

    switch (charFormat->GetSelection()) {
        case 0:
            chFormat = Serial::SerialCharacterFormat::SEVEN_BITS;
            break;
        case 1:
            chFormat = Serial::SerialCharacterFormat::EIGHT_BITS;
            break;
    }

    switch (terminatorCtrl->GetSelection()) {
        case 0:
            terminator = Serial::SerialPortTerminator::NO_TERMINATOR;
            break;
        case 1:
            terminator = Serial::SerialPortTerminator::CR;
            break;
        case 2:
            terminator = Serial::SerialPortTerminator::LF;
            break;
        case 3:
            terminator = Serial::SerialPortTerminator::CR_LF;
            break;
    }

    _connectedPort = std::make_shared<Serial::SerialPort>(std::string(portSelector->GetStringSelection().mb_str()), 1000);

    if (_connectedPort->openPort() != Serial::SerialPortErrorCode::NO_ERROR) {
        wxMessageDialog *errordia = new wxMessageDialog(NULL, wxT("Error while opening port, it may be busy"), wxT("Error"), wxOK | wxICON_ERROR);
        errordia->ShowModal();   
        return;
    }
    // _connectedPort->configurePort(B9600);
    if (_connectedPort->configurePortWithCustomSettings(rate, flControl, terminator, chFormat, chControl, chStops ) != Serial::SerialPortErrorCode::NO_ERROR) {
        wxMessageDialog *errordia = new wxMessageDialog(NULL, wxT("Error while configurating port"), wxT("Error"), wxOK | wxICON_ERROR);
        errordia->ShowModal();   
        return;
    }
    // connectedPort->writeMessage("Hello from gui ;)");
    // connectedPort->closePort();
    
    // Otwiarcie okna polaczenia z wybranym portem
    ConnectionWindow *connWindow = new ConnectionWindow("Connection with [" + std::string(portSelector->GetStringSelection().mb_str()) + "]", 
                                                            wxSize(800, 600), _connectedPort);
    // zamkniecie okna 
    //this->Close();
    // otwarcie okna
    connWindow->Show();
}

void MainWindow::pingPort() {
    double rate = 0.0;
    try {
         rate = std::stod(std::string(baudRateBox->GetValue().mb_str()), nullptr);
    } catch (std::invalid_argument& ex) {
        wxMessageDialog *errordia = new wxMessageDialog(NULL, wxT("Baudrate in incorrect format"), wxT("Error"), wxOK | wxICON_ERROR);
        errordia->ShowModal();   
        return;
    }

    Serial::SerialFlowControl flControl;
    Serial::SerialPortTerminator terminator;
    Serial::SerialCharacterFormat chFormat;
    Serial::SerialCharacterControl chControl;
    Serial::SerialCharacterStopBits chStops;

    switch (flowCtrl->GetSelection()) {
        case 0:
            flControl = Serial::SerialFlowControl::NO_CONTROL;
            break;
        case 1:
            flControl = Serial::SerialFlowControl::HARDWARE_DTR_DSR;
            break;
        case 2:
            flControl = Serial::SerialFlowControl::HARDWARE_RTS_CTS;
            break;
        case 3:
            flControl = Serial::SerialFlowControl::SOFTWARRE_XON_XOFF;
            break;
    }

    switch (charStop->GetSelection()) {
        case 0:
            chStops = Serial::SerialCharacterStopBits::ONE;
            break;
        case 1:
            chStops = Serial::SerialCharacterStopBits::TWO;
            break;
    }

    //even odd none
    switch (charControl->GetSelection()) {
        case 0:
            chControl = Serial::SerialCharacterControl::EVEN;
            break;
        case 1:
            chControl = Serial::SerialCharacterControl::ODD;
            break;
        case 2:
            chControl = Serial::SerialCharacterControl::NONE;
            break;
    }

    switch (charFormat->GetSelection()) {
        case 0:
            chFormat = Serial::SerialCharacterFormat::SEVEN_BITS;
            break;
        case 1:
            chFormat = Serial::SerialCharacterFormat::EIGHT_BITS;
            break;
    }

    switch (terminatorCtrl->GetSelection()) {
        case 0:
            terminator = Serial::SerialPortTerminator::NO_TERMINATOR;
            break;
        case 1:
            terminator = Serial::SerialPortTerminator::CR;
            break;
        case 2:
            terminator = Serial::SerialPortTerminator::LF;
            break;
        case 3:
            terminator = Serial::SerialPortTerminator::CR_LF;
            break;
    }

    _connectedPort = std::make_shared<Serial::SerialPort>(std::string(portSelector->GetStringSelection().mb_str()), 1000);

    if (_connectedPort->openPort() != Serial::SerialPortErrorCode::NO_ERROR) {
        wxMessageDialog *errordia = new wxMessageDialog(NULL, wxT("Error while opening port, it may be busy"), wxT("Error"), wxOK | wxICON_ERROR);
        errordia->ShowModal();   
        return;
    }

    if (_connectedPort->configurePortWithCustomSettings(rate, flControl, terminator, chFormat, chControl, chStops ) != Serial::SerialPortErrorCode::NO_ERROR) {
        wxMessageDialog *errordia = new wxMessageDialog(NULL, wxT("Error while configurating port"), wxT("Error"), wxOK | wxICON_ERROR);
        errordia->ShowModal();   
        return;
    }

    //std::cout << "Available: " << connectedPort->available() << std::endl;

    //connectedPort->configurePortWithCustomSettings(rate,flControl, terminator, chFormat, chControl, chStops);
    
    double time = _connectedPort->ping();

    if (time < 0) {
        wxMessageDialog *errordia = new wxMessageDialog(NULL, wxT("Ping was unsuccessful. Try changing port."), 
                                                            wxT("Error"), wxOK | wxICON_ERROR);
        errordia->ShowModal();  
        return;
    }

    using boost::format;

    wxMessageDialog *pingDia = new wxMessageDialog(NULL, wxT("Measured Round-trip delay: " + (format("%1$.2f") % time).str() + " ms."),
                                                         wxT("Ping was successful!"), wxOK | wxICON_INFORMATION);
    pingDia->ShowModal();  
    
    _connectedPort->closePort();
    _connectedPort.reset();
}

void MainWindow::refreshPortList() {

    portSelector->Clear();

    std::vector<std::string> serialPorts = Serial::SerialPort::scanAvailablePorts();
    int i = 0;
    for (const auto & file : serialPorts) {
        portSelector->Insert(file, i++);
    }
}