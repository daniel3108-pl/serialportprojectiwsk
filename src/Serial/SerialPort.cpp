#include "../../include/Serial/SerialEnums.h"
#include "../../include/Serial/SerialPort.h"
#include "../../include/Serial/Exceptions.h"

namespace Serial {

    SerialPort::SerialPort(const std::string name, const double timeout) 
        : _portName(name), _timeout(timeout)
    {
        this->_com = -1;
    }

    SerialPort::SerialPort(const SerialPort& port) 
        : _portName(port._portName), _com(port._com), _serialPortOptions(port._serialPortOptions), _timeout(port._timeout)
    {
    }

    SerialPort::~SerialPort() {
        
    }

    #pragma region Funkcjonalność obsługi portu

    SerialPortErrorCode SerialPort::openPort() {

        std::string pathToPort = "/dev/" + this->_portName;
        const char* pathToPortCStr = pathToPort.c_str();

        // otwarcie polaczenia z portem szeregowym o podanej nazwie
        int port = open(pathToPortCStr, O_RDWR | O_NONBLOCK | O_NOCTTY);

        if (port <= 0) {
            int error = errno;
            std::cerr << "[Error] Could not open port: " << pathToPort << "." << std::endl;
            std::cerr << "[Error Message] " << strerror(error) << std::endl;

            return error != 16 ? SerialPortErrorCode::NO_SUCH_PORT : SerialPortErrorCode::PORT_BUSY;
        }
        this->_com = port;

        return SerialPortErrorCode::NO_ERROR;
    }

    SerialPortErrorCode SerialPort::configurePort(const int bitrate) {

        if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));

        struct termios ttyPort;

        // Zbindowanie struktury z otwarym przez nas portem
        if (tcgetattr(this->_com, &ttyPort) != 0) {
            std::cerr << "[Error] Could not configure port: " << this->_portName << "." << std::endl;
            std::cerr << "[Error Message] " << strerror(errno) << std::endl;

            return SerialPortErrorCode::CONFIGURATION_ERROR;
        }

        // ustawianie szybkosci na wejsciu i wyjsciu dla portu 
        // za pomoca podanego bitrateu (baudrate'u)
        cfsetispeed(&ttyPort, bitrate);
        cfsetospeed(&ttyPort, bitrate);

        // Ustawiamy konretne domyslne flagi dla portu szeregowego
        // dokladnie omowione jest to w metodzie configurePort with custom settings
        ttyPort.c_cflag |= (CLOCAL | CREAD);
        ttyPort.c_cflag |= PARENB;
        ttyPort.c_cflag &= ~CSIZE;
        ttyPort.c_cflag |= CS8;

        ttyPort.c_lflag &= ~ICANON;
        ttyPort.c_lflag &= ~ECHO;
        ttyPort.c_lflag &= ~ECHOE;
        ttyPort.c_lflag &= ~ECHONL;
        ttyPort.c_lflag &= ~ISIG;

        ttyPort.c_iflag &= ~(IXON | IXOFF | IXANY);
        ttyPort.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);

        ttyPort.c_oflag &= ~OPOST;
        ttyPort.c_oflag &= ~ONLCR;

        ttyPort.c_cc[VTIME] = 10;
        ttyPort.c_cc[VMIN] = 0;

        // Zatwierdzenie i zapisanie w polaczeniu z portem ustawionych przez nas flag
        if (tcsetattr(this->_com, TCSANOW, &ttyPort) != 0) {
            std::cerr << "[Error] Could not configure port: " << this->_portName << "." << std::endl;
            std::cerr << "[Error Message] " << strerror(errno) << std::endl;

            return SerialPortErrorCode::CONFIGURATION_ERROR;
        }

        this->_serialPortOptions = ttyPort;

        return SerialPortErrorCode::NO_ERROR;
    }

    SerialPortErrorCode SerialPort::configurePortWithCustomSettings(const unsigned int baudrate, 
                                                    SerialFlowControl flControl, 
                                                    SerialPortTerminator terminator,
                                                    SerialCharacterFormat chFormat,
                                                    SerialCharacterControl chControl,
                                                    SerialCharacterStopBits chStops) {
        
        if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));
    
        struct termios ttyPort;

        if (tcgetattr(this->_com, &ttyPort) != 0) {
            std::cerr << "[Error] Could not configure port: " << this->_portName << "." << std::endl;
            std::cerr << "[Error Message] " << strerror(errno) << std::endl;

            return SerialPortErrorCode::CONFIGURATION_ERROR;
        }
        // ustawienie na wejsciu i wyjsciu danych baudrate
        cfsetispeed(&ttyPort, baudrate);
        cfsetospeed(&ttyPort, baudrate);

        // Czysci ustawiony rozmiar (bity danych)
        
        ttyPort.c_cflag |= (CLOCAL | CREAD);

        switch(flControl) {
            case SerialFlowControl::NO_CONTROL:
                ttyPort.c_cflag &= ~CRTSCTS;
                break;
            case SerialFlowControl::HARDWARE_RTS_CTS:
                ttyPort.c_cflag |= CRTSCTS;
                break;
            case SerialFlowControl::HARDWARE_DTR_DSR:
                #if __APPLE__
                    ttyPort.c_cflag |= (CDTR_IFLOW | CDSR_OFLOW);
                #else
                    ttyPort.c_cflag |= (TIOCM_DTR);
                #endif
                break;
            case SerialFlowControl::SOFTWARRE_XON_XOFF:
                ttyPort.c_iflag |= (IXON | IXOFF | IXANY);
                break;
        }


        switch(chFormat) {
            case SerialCharacterFormat::SEVEN_BITS:
                ttyPort.c_cflag &= ~CSIZE;
                ttyPort.c_cflag |= CS7; // ustawia bity danych na 7
                break;
            case SerialCharacterFormat::EIGHT_BITS:
                ttyPort.c_cflag &= ~CSIZE;
                ttyPort.c_cflag |= CS8; // ustawia bity danych na 8
                break;
        }
        // ustawia parzystosc
        switch(chControl) {
            case SerialCharacterControl::NONE:
                ttyPort.c_cflag &= ~(PARENB); // wylacza sprawdzanie parzystosci
                ttyPort.c_iflag &= ~(INPCK);
                break;
            case SerialCharacterControl::EVEN:
                ttyPort.c_cflag |= PARENB; // wlacza parzystosc
                ttyPort.c_cflag &= ~(PARODD); // czyszczenie parzystosci nieparzystej (Heh) dajac parzysta
                ttyPort.c_iflag |= (INPCK);
                break;
            case SerialCharacterControl::ODD:
                ttyPort.c_cflag |= (PARENB | PARODD); 
                ttyPort.c_iflag |= (INPCK);
                break;
        }
        // ustawia stop bitow
        switch(chStops) {
            case SerialCharacterStopBits::ONE:
                ttyPort.c_cflag &= ~CSTOPB;
                break;
            case SerialCharacterStopBits::TWO:
                ttyPort.c_cflag |= CSTOPB;
                break;
        }
        
        //ttyPort.c_oflag &= ~OPOST;
        ttyPort.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
        switch (terminator) {
            case SerialPortTerminator::NO_TERMINATOR:
                ttyPort.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
                _terminator = "";
                break;
            case SerialPortTerminator::CR:
                // ttyPort.c_lflag |= (ICANON | ISIG);
                // ttyPort.c_cc[VEOL] = '\r';
                _terminator = "\\r";
                break;
            case SerialPortTerminator::CR_LF:
                // ttyPort.c_lflag |= (ICANON | ISIG);
                // ttyPort.c_cc[VEOF] = 0x04;
                // ttyPort.c_cc[VEOL] = '\r';
                // ttyPort.c_cc[VEOL2] = '\n';
                _terminator = "\\r\\n";
                break;
            case SerialPortTerminator::LF:
                // ttyPort.c_lflag |= (ICANON | ISIG);
                // ttyPort.c_cc[VEOL] = '\n';
                _terminator = "\\n";
                break;
        }
        
        ttyPort.c_oflag &= ~ONLCR;
        ttyPort.c_oflag |= OPOST;

        ttyPort.c_cc[VTIME] = 10;
        ttyPort.c_cc[VMIN] = 0;

        tcflush(this->_com, TCIFLUSH);
        if (tcsetattr(this->_com, TCSANOW, &ttyPort) != 0) {
            std::cerr << "[Error] Could not configure port: " << this->_portName << "." << std::endl;
            std::cerr << "[Error Message] " << strerror(errno) << std::endl;

            return SerialPortErrorCode::CONFIGURATION_ERROR;
        }

        this->_serialPortOptions = ttyPort;

        return SerialPortErrorCode::NO_ERROR;
    }   

    bool SerialPort::closePort() {
        close(this->_com);
        
        return true;
    }

    double SerialPort::ping() {
        if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));

        this->clearMessages();
        
        auto start = std::chrono::steady_clock::now();

        this->writeMessage("PING");

        // oczekiwanie na wiadomosc od portu, ktory pingujemy
        std::string msg = this->readMessage();
        while(msg == "") {
            msg = this->readMessage();

            if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() > this->_timeout)
                return -1.0;
        }

        // jesli otrzymana odpowiedz to nie OK, znaczy ze cos sie zepsulo
        if (msg.find("OK") == std::string::npos) {
            return -1.0;
        }

        auto end = std::chrono::steady_clock::now();

        // zwraca czas w mikrosekundach policzonego za pomoca czasow startu i konca metody
        return std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    
    }

    std::string SerialPort::readMessage() {

        if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));

        std::string read_buf("", 255);

        // Odczytanie buffora danych, zwraca jednorazowo tyle bytow ile ustawilismy w configuracji
        int n = read(this->_com, &read_buf[0], 255);

        if (n <= 0) {
            return "";
        }
        
        std::string readData(&read_buf[0], n);

        if (_terminator != "" && !readData.ends_with(_terminator)) {
            _currentMessageBeforeTerminator += readData;
            readData = "";
        } else {
            readData = _currentMessageBeforeTerminator + readData + "\n";
            _currentMessageBeforeTerminator = "";
        }

        return readData;
    }

    std::string SerialPort::readMessage(int bytes) {

        if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));

        std::string read_buf = "";

        int n = read(this->_com, &read_buf[0], bytes);

        if (n <= 0) {
            return "";
        }

        std::string readData(&read_buf[0], n);


        if (_terminator != "" && !readData.ends_with(_terminator)) {
            _currentMessageBeforeTerminator += readData;
            readData = "";
        } else {
            readData = _currentMessageBeforeTerminator + readData + "\n";
            _currentMessageBeforeTerminator = "";
        }
        return readData;
    }

    unsigned char* SerialPort::readBytes() {

        if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));

        unsigned char* read_buf = new unsigned char[255];

        int n = read(this->_com, read_buf, sizeof(read_buf));

        if (n <= 0) {
            return nullptr;
        }
        return read_buf;
    }

    unsigned char* SerialPort::readBytes(int bytes) {

        if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));

        unsigned char* read_buf = new unsigned char[bytes];

        int n = read(this->_com, read_buf, sizeof(read_buf));

        if (n <= 0) {
            return nullptr;
        }
        return read_buf;
    }

    bool SerialPort::clearMessages() {
         if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));

        while(this->readMessage() != "") {}

        return true;
    }

    bool SerialPort::writeMessage(const std::string& message) {
        std::string toSend = message + this->_terminator;

        if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));

        if (write(_com, toSend.c_str(), toSend.size()) == -1)
            return false;

        return true;
    }

    int SerialPort::available() {
         if (this->_com <= 0)
            throw Serial::SerialPortNotOpenException("Serial port " + this->_portName + " is not open. File descriptor is " + std::to_string(this->_com));
        
        int ret = 0;
        ioctl(_com, FIONREAD, &ret);
        return ret;
    }

    #pragma endregion

    #pragma region Gettery Settery

    std::string SerialPort::getPortName() {
        return this->_portName;
    }

    int SerialPort::getCom() {
        return this->_com;
    }

    struct termios SerialPort::getSerialPortOptions() {
        return this->_serialPortOptions;
    }

    #pragma endregion

    std::vector<std::string> SerialPort::scanAvailablePorts() {
        std::vector<std::string> files;
        std::string path = "/dev";
        // regex do sprawdzenia plikow ttys
        #if __APPLE__
            std::regex fReg("((tty|cu)\\.).+");
        #elif __linux__
            std::regex fReg("tty(S|USB)[0-9]+");
        #endif

        // przechodzi po katalogu /dev/ i szuka wszystkich plikow z wzorem ttys w nazwie (ttys oznacza port szeregowy)
        for (const auto & file : std::filesystem::directory_iterator(path)) {
            // Pobranie nazwy pliku do zwyklego stringa
            std::string filePath = file.path().filename().generic_string();
            if (std::regex_search(filePath, fReg)) {
                files.push_back(filePath);
            }
        }
        
        return files;
    }

}

