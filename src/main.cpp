#include "../include/main.h"

// Inicjalizuje aplikacje
IMPLEMENT_APP(App)

bool App::OnInit() {
    // tworzenie okna wejsciowego
    MainWindow *mainWin = new MainWindow(wxT("Serial Port Communication Tool"), wxSize(800, 600));
    // wyswietlenie okna wejsciowego
    mainWin->Show(true);

    return true;
}

