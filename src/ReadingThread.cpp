#include "../include/ReadingThread.h"


ReadingThread::ReadingThread(wxFrame* handler, std::shared_ptr<Serial::SerialPort> port) 
    : wxThread(wxTHREAD_DETACHED), _handler(handler), _port(port)
{
}

ReadingThread::~ReadingThread() {
}

wxThread::ExitCode ReadingThread::Entry() {

    if (_port->getCom() == -1)
        return (wxThread::ExitCode) -1;

    while (!TestDestroy()) {
        
        std::string message = _port->readMessage();

        if (message != "") {
            
            bool wasPing = false;

            if (message.find("PING") != std::string::npos) {
                std::cout << "Ping" << std::endl;
                _port->writeMessage("OK");
                wasPing = true;
            }

            if (!wasPing) {
                wxCommandEvent event(wxEVT_COMMAND_TEXT_UPDATED, READING_UPDATED);
                event.SetString(message);
                _handler->GetEventHandler()->AddPendingEvent(event);
            }
        }

        this->Sleep(20);
    }

    return (wxThread::ExitCode) 0;
}

