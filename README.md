# IWSK - Zadanie 1 - Komunikacja na porcie znakowym

## Wazne 

Projekt jest skierowany do systemow UNIX'owych t.j. macOS oraz Linux, nie zadziała on na systemie Windows ze względu na korzystanie z funkjci POSIXowych z bibliotek systemowych, których nie ma na Windows'ach.

## Struktura projektu

> /src/*.cpp -  pliki źródłowe .cpp dla klas związanych z GUI

> /src/Serial/*.cpp - pliki źródłowe .cpp dla klas związanych z obsługą portu znakowego

> /include/*.h - pliki nagłówkowe .h dla klas związanych z GUI programu

> /include/Serial/*.h - pliki nagłówkowe .h dla klas związanych z obsługą portu szeregowego

## Kompilacja i Uruchamianie programu 

Przed skompilowaniem musimy miec zainstalowane narzędzie CMake oraz bibliotekę GUI wxWidgets w jednym z głównych katalogów do bilbiotek c++, np. w /usr/local/,

- [Strona z biblioteką do GUI](https://www.wxwidgets.org)
- [Strona CMake'a](https://www.wxwidgets.org)

### Kompilacja 

Mając juz zainstalowaną bibliotekę i narzędzie CMake w katalogu głównym programu (z plikiem CMakeLists.txt) uruchamiamy komendę:
```
mkdir build && cd build
cmake .. & cmake --build . --target all
```

program uruchamiamy komendą w folderze build:
```
./SerialPortIWSK
```

Jeśli chcemy szybko stworzyc wirtualne porty szeregowe korzystamy ze skryptu "emulate_serial.sh" w głównym katalogu projektu.

Zeby skorzystac z tego skryptu musimy miec zainstalowany program "socat", ktory mozna latwo zainstalowac np. w apt-get czy homebrew, jego binarka musi byc tez podpieta do sciezki PATH.
